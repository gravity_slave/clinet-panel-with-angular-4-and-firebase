import { Component, OnInit } from '@angular/core';
import {Client} from '../../models/client';
import {Router} from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import {ClientService} from '../../services/client.service';
import {SettingsService} from "../../services/settings.service";

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {
  client: Client = {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    balance: 0
  };

  disableBalanceOnAdd = false;

  constructor(
    private flashMessagesService: FlashMessagesService,
    private clientService: ClientService,
    private router: Router,
    private settingService: SettingsService
  ) { }

  ngOnInit() {
    this.disableBalanceOnAdd = this.settingService.getSettings().disableBalanceOnAdd;
  }

  onSubmit({value, valid}: {value: Client, valid: Boolean}) {
    if (this.disableBalanceOnAdd) {
      value.balance = 0;
    }
    if (!valid) {
      this.flashMessagesService.show('Please fill in all fields', {cssClass: 'alert alert-danger', timeOut: 4000});
      this.router.navigate(['add-client']);
    } else {
      this.clientService.newClient(value);
      this.flashMessagesService.show('New Client has been Added!', {cssClass: 'alert alert-success', timeOut: 4000});
      this.router.navigate(['/']);
    }
  }

}
