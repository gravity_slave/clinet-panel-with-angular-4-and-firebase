import { Component, OnInit } from '@angular/core';
import {ClientService} from '../../services/client.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {ActivatedRoute, Router} from '@angular/router';
import {Client} from '../../models/client';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {
  id: string;
  client: Client;
  hasBalance = false;
  showBalanceUpdateInput = false;

  constructor(private clientService: ClientService,
              private flashMessagesService: FlashMessagesService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.clientService.getClient(this.id).subscribe(client => {
      if (client.balance > 0) {
        this.hasBalance = true;
      }
      this.client = client;
      console.log(this.client);
    });
  }

  onDeleteClick() {

   if(confirm('are you sure to delete this client?')) {
     this.clientService.deleteClient(this.id);
     this.flashMessagesService.show('The client has been deleted', {cssClass: 'alert alert-danger', timeout: 4000});
     this.router.navigate(['/']);
   }
}

  updateBalance(id: string) {
    this.clientService.updateClient(this.id, this.client);
    this.flashMessagesService.show('Balance Updated', {cssClass: 'alert alert-success', timeout: 4000});
    this.router.navigate(['/clients/', this.id]);
  }

}
