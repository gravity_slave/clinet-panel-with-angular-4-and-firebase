import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SettingsService } from '../../services/settings.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Settings } from '../../models/settings';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
    settings: Settings;
  constructor(
    private settingService: SettingsService,
    private flashMessagesService: FlashMessagesService,
    private router: Router
  ) { }

  ngOnInit() {
   this.settings = this.settingService.getSettings();
  }
  onSubmit() {
    this.settingService.changeSettings(this.settings);
    this.flashMessagesService.show('Settings saved', {cssClass: 'alert alert-info', timeout: 4000});
    this.router.navigate(['/settings']);
  }

}
