import { Component, OnInit } from '@angular/core';
import {ClientService} from '../../services/client.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {ActivatedRoute, Router} from '@angular/router';
import {Client} from '../../models/client';
import {SettingsService} from "../../services/settings.service";

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.css']
})
export class EditClientComponent implements OnInit {
  id: string;
  client: Client= {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    balance: 0
  };

  disableBalanceOnEdit = true;

  constructor(private clientService: ClientService,
              private flashMessagesService: FlashMessagesService,
              private settingService: SettingsService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.disableBalanceOnEdit = this.settingService.getSettings().disableBalanceOnEdit;
    this.id = this.route.snapshot.params['id'];
    this.clientService.getClient(this.id).subscribe( client => this.client = client );
  }

  onSubmit({value, valid}: {value: Client, valid: boolean}) {
    if (!valid) {
      this.flashMessagesService.show('Please fill in all fields', {cssClass: 'alert alert-danger', timeOut: 4000});
      this.router.navigate(['edit-client/', this.id]);
    } else {
      this.clientService.updateClient(this.id, value);
      this.flashMessagesService.show('The Client has been Updated!', {cssClass: 'alert alert-success', timeOut: 4000});
      this.router.navigate(['/client/', this.id]);

    }
  }
}
