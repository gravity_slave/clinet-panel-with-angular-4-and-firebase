import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import 'rxjs/add/operator/map';
@Injectable()
export class AuthService {

  constructor(
    public afAuth: AngularFireAuth
  ) { }

  login(email, password: string) {
    return new Promise( (res, rej) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, password).then(
        data => res(data),
        err => rej(err)
      )
    })
  }

  checkUserStatus() {
    return this.afAuth.authState.map( auth =>  auth );
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  register(email, password: string) {
    return new Promise( (resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, password)
        .then( userData => resolve(userData),
        err => reject(err));
    })
  }
}
